import { addLocaleData } from 'react-intl';
import pt from 'react-intl/locale-data/pt';

addLocaleData([...pt]);

const i18nData = {
  locale: 'pt-BR',
};

export default i18nData;
