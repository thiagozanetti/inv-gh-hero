import { connect } from 'react-redux';

import ContentAreaComponent from './content-area';

const stateToProps = () => ({

});

const dispatchToProps = () => ({

});

const ContentArea = connect(stateToProps, dispatchToProps)(ContentAreaComponent);

export default ContentArea;
