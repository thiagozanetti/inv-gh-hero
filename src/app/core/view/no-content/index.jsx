import React from 'react'
import FlexBox from 'flexbox-react'

import './no-content.css'

const NoContent = () => {
  return (
    <FlexBox className='no-content-container' />
  );
};

export default NoContent;
