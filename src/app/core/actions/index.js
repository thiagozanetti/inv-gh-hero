const getApiUrl = () => process.env.REACT_APP_API_URL || 'https://api.github.com';

export default getApiUrl;
