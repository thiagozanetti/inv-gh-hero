import React from 'react';
import ReactDOM from 'react-dom';

import AppBootstrap from './bootstrap';

import './index.css';

ReactDOM.render(<AppBootstrap/>,
  document.getElementById('app'));

